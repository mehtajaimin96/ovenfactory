package OvenFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class OvenFactoryTest {

    static WebDriver driver;
    static WebDriverWait wait;

    @BeforeTest
    public void setup() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        options.addArguments("--enable-javascript");
        options.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get("https://www.ovenstory.in/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 30);

    }
// TC 1 - To add product into cart.
    @Test
    public void test1(){
        driver.findElement(By.cssSelector("input.locationSearchInput")).sendKeys("Kandivali");
        driver.findElement(By.cssSelector("input.locationSearchInput")).click();
        driver.findElement(By.id("suggestionMainText")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.id("addProductCombo")));
        List<WebElement> list = driver.findElements(By.id("addProductCombo"));
        list.get(0).click();
        System.out.println("Number of add Button Found----->"+list.size());
        driver.findElement(By.cssSelector("a.primaryBtn.changeCustomisationSetBtn")).click();
        driver.findElement(By.cssSelector("a.primaryBtn.changeCustomisationSetBtn")).click();
        driver.findElement(By.cssSelector("a.primaryBtn.checkoutBtn")).click();


    }
// TC 2 - To click on every side menu option.
@Test
public void test2(){
    driver.findElement(By.cssSelector("a.headerIcon")).click();
    driver.findElement(By.id("sb_col")).click();
    System.out.println(driver.getCurrentUrl());
    driver.navigate().back();
    driver.findElement(By.cssSelector("a.headerIcon")).click();
    driver.findElement(By.id("sb_po")).click();
    System.out.println(driver.getCurrentUrl());
    driver.navigate().back();
    driver.findElement(By.cssSelector("a.headerIcon")).click();
    driver.findElement(By.id("sb_tr")).click();
    System.out.println(driver.getCurrentUrl());
    driver.navigate().back();
    driver.findElement(By.cssSelector("a.headerIcon")).click();
    driver.findElement(By.id("sb_pr")).click();
    System.out.println(driver.getCurrentUrl());
    driver.navigate().back();
    driver.navigate().back();


}
//TC 3 - To change location from collection page
@Test
public void test3()
{
    driver.navigate().to(driver.getCurrentUrl());
    driver.findElement(By.className("locationNameHeader")).click();
    driver.findElement(By.cssSelector("input.locationSearchInput")).sendKeys("Powai");
    driver.findElement(By.cssSelector("input.locationSearchInput")).click();
    driver.findElement(By.id("suggestionMainText")).click();

}

    @AfterTest
    public void teardown() {
        driver.quit();
    }
}